package com.ulan.model;

public enum Users {
    ADMIN,
    SAFETY_ENGINEER,
    DEPARTMENT_HEAD,
    VIEWER
}
