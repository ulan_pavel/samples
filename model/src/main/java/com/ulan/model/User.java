package com.ulan.model;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;


@Table(name = "User")
@Entity
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String login;
    @Column
    private String password;
    @Column
    private String role;
    @Column
    private Date lastVisit;

   /* @OneToMany(mappedBy = "User",cascade = CascadeType.ALL, orphanRemoval = true)
    public*/

    public User(String login, String password, Date lastVisit) {
        this.login = login;
        this.role = "viewer";

        this.password = password;
        this.lastVisit = lastVisit;
    }

    public User(String login, String password, String role, Date lastVisit) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.lastVisit = lastVisit;
    }

    public User(Long id, String login, String password, String role, Date lastVisit) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
        this.lastVisit = lastVisit;
    }

    public User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getId().equals(user.getId()) &&
                getLogin().equals(user.getLogin()) &&
                getPassword().equals(user.getPassword()) &&
                getRole().equals(user.getRole()) &&
                getLastVisit().equals(user.getLastVisit());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLogin(), getPassword(), getRole(), getLastVisit());
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", lastVisit=" + lastVisit +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(Date lastVisit) {
        this.lastVisit = lastVisit;
    }
}
