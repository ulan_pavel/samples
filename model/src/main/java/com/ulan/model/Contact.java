package com.ulan.model;

import java.util.Objects;


public class Contact {
    private Long id;
    private String login;
    private String password;
    private Users role;

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

    public Contact(Long id, String login, String password, Users role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role=role;

    }

    public Contact() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return id.equals(contact.id) &&
                login.equals(contact.login) &&
                password.equals(contact.password) &&
                role == contact.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, role);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Users getRole() {
        return role;
    }

    public void setRole(Users role) {
        this.role = role;
    }

    public void setRole(String role) throws RoleException{
        if ((role.toUpperCase().equals(Users.ADMIN.toString())) || (role.equals(Users.DEPARTMENT_HEAD.toString()))
                || (role.equals(Users.SAFETY_ENGINEER.toString())) || (role.equals(Users.VIEWER.toString()))) {
        } else throw new RoleException("Роль пользователя не соответствует ни одной из существующих");
    }
}

class RoleException extends Exception {
    public RoleException(String message) {
        super(message);
    }
}
