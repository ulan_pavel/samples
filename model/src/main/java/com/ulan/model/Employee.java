package com.ulan.model;

import javafx.util.converter.IntegerStringConverter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

public class Employee {
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private String profession;
    private String department;
    private String date;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String middleName, String email, String profession, String department) {
        GregorianCalendar date = new GregorianCalendar();
        IntegerStringConverter converter = new IntegerStringConverter();
        this.date = converter.toString(date.get(Calendar.YEAR)) + "-" + converter.toString(date.get(Calendar.MONTH) + 1) + "-" + converter.toString(date.get(Calendar.DAY_OF_MONTH));
    }


    @Override
    public String toString() {
        return
                "Имя:" + firstName + '\'' +
                        ", Фамилия:" + lastName + '\'' +
                        ", Отчество:" + middleName + '\'' +
                        ", email:" + email + '\'' +
                        ", должность:" + profession + '\'' +
                        ", отдел:" + department + '\'' +
                        ", дата трудоустройства:" + date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return getFirstName().equals(employee.getFirstName()) &&
                getLastName().equals(employee.getLastName()) &&
                Objects.equals(getMiddleName(), employee.getMiddleName()) &&
                Objects.equals(getEmail(), employee.getEmail()) &&
                getProfession().equals(employee.getProfession()) &&
                getDepartment().equals(employee.getDepartment()) &&
                Objects.equals(getDate(), employee.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getMiddleName(), getEmail(), getProfession(), getDepartment(), getDate());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
