package com.ulan.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


@WebFilter(urlPatterns = {"/*"},
        filterName = "authFilter",
        initParams = {
                @WebInitParam(name = "active", value = "true", description = "authentication activation")
        })

public class AuhenticationFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(AuhenticationFilter.class.getName());

    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(
            new HashSet<>(Arrays.asList("/login.jsp", "/index.html", "/auth", "/error", "/css", "/img"))
    );

    private static final String ACTIVE_INIT_PARAM = "true";
    private static final String ROLE = "role";

    private boolean active;

    @Override
    public void init(FilterConfig config) throws ServletException {
        String active = config.getInitParameter(ACTIVE_INIT_PARAM);
        this.active = active != null && active.toLowerCase().equals("true");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        LOGGER.info("auth filter before doFilter is running");
        if (active) {

            if (bypassFilter(req) || hasValidSession(req)) {
                chain.doFilter(request, response);
                LOGGER.info("in if bypass & session");
            } else {
                LOGGER.error("redirect");
                res.sendError(401);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        //do nothing
    }

    private boolean bypassFilter(HttpServletRequest req) {
        String contextPath = req.getContextPath();
        String path = req.getRequestURI().replaceFirst(contextPath, "");
        return ALLOWED_PATHS.stream().anyMatch(path::equals);
    }

    private boolean hasValidSession(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return session != null && session.getAttribute(ROLE) != null;
    }
}
