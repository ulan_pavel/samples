package com.ulan.servlet;

import com.ulan.model.Employee;
import com.ulan.services.impl.AddUserImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class AddServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //FIXME just for test, will delete later
        PrintWriter writer = resp.getWriter();
        writer.println("Method GET from AddServlet");
    }

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String middleName = req.getParameter("middleName");
        String email = req.getParameter("email");
        String profession = req.getParameter("profession");
        String department = req.getParameter("department");
        Date date1 = new Date();
        String date = date1.toString();

        Employee employee = new Employee();
        employee.setFirstName(firstName);
        employee.setMiddleName(middleName);
        employee.setLastName(lastName);
        employee.setEmail(email);
        employee.setProfession(profession);
        employee.setDepartment(department);
        employee.setDate(date);

        AddUserImpl addUser = new AddUserImpl();
        boolean redirect = addUser.addUser(employee);
        if (redirect) {
            String contextPath = req.getContextPath();
            String path = (contextPath + "list.jsp");
            resp.sendRedirect(path);
        } else {
            String contextPath = req.getContextPath();
            String path = (contextPath + "errors/404.jsp");

        }
    }
}
