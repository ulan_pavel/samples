package com.ulan.servlet;

import com.ulan.model.Contact;
import com.ulan.model.Users;
import com.ulan.services.AuthorizationService;
import com.ulan.services.impl.AuthorizationServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "login",
        urlPatterns = {"/auth"})

public class LoginServlet extends HttpServlet {

    private AuthorizationService authorizationService = new AuthorizationServiceImpl();

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        String contextPath = req.getContextPath();
        /* FIXME Вынести в БД (или в отдельный файл со считыванием/записью) */
        Contact admin = new Contact(1L, "admin", "admin", Users.ADMIN);
        Contact department = new Contact(2L, "department", "department", Users.DEPARTMENT_HEAD);
        Contact safe = new Contact(3L, "safe", "safe", Users.SAFETY_ENGINEER);
        Contact viewer = new Contact(4L, "viewer", "viewer", Users.VIEWER);

        ArrayList<Contact> list = new ArrayList<>();
        list.add(admin);
        list.add(department);
        list.add(safe);
        list.add(viewer);

        String path = "";
        for (Contact user : list) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                HttpSession session = req.getSession();
                session.setMaxInactiveInterval(300);//время бездействия - 5 мин
                session.setAttribute("role", user.getRole());
                path = (contextPath + "main.jsp");
                break;
            } else path = (contextPath + "login.jsp");
        }
        resp.sendRedirect(path);
    }
}