<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 18.02.2020
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="com.ulan.model.Users" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>List of employers</title>
</head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="css/index.css">

<body>
<header>
    <%--<a href="logo.png" class="logo">logo</a>--%>
        <nav>
            <ul class="topmenu">
                <% if (session.getAttribute("role").equals(Users.ADMIN) || session.getAttribute("role").equals(Users.SAFETY_ENGINEER)) {%>
                <%@ include file="menus/staff.jsp" %>
                <%@ include file="menus/instructions.jsp" %>
                <%}  %>
                <% if (session.getAttribute("role").equals(Users.DEPARTMENT_HEAD)) {%>
                <%@ include file="menus/instructions.jsp" %>
                <%}  %>

                <li><a href="main.jsp" class="">Главная<span class="fa fa-angle-down"></span></a>
                    <ul class="submenu">
                        <li><a href="">меню второго уровня</a>
                        </li>

                        <li><a href="">меню второго уровня<span class="fa fa-angle-down"></span></a>
                            <ul class="submenu">
                                <li><a href="">меню третьего уровня</a></li>
                                <li><a href="">меню третьего уровня</a></li>
                                <li><a href="">меню третьего уровня</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li><a href="${pageContext.request.contextPath}login.jsp">Логин</a></li>
                <li><a href="contacts.jsp">Контакты</a></li>
                <li><a href="/logout">Выход</a></li>
            </ul>
        </nav>
</header>

<div>
    Ваш уровень доступа:<i><%= session.getAttribute("role").toString().toLowerCase() %></i>
</div>
    <style>
      .space {
      color: black;
      margin-left: 10px;
      }
    </style>
  <legend class=space>Список сотрудников</legend>

          <div class=space>

          <c:set var="list"  value="${message}" />

          <c:forEach items="${list}" var="message">
              <li><c:out value="${message}"/></li>
          </c:forEach>

</body>
</html>
