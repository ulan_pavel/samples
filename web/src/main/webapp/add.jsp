<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 18.02.2020
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.ulan.model.Users" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Работа с сущностями</title>
</head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="css/index.css">

<body>
<header>
    <%--<a href="logo.png" class="logo">logo</a>--%>
        <nav>
            <ul class="topmenu">
                <% if (session.getAttribute("role").equals(Users.ADMIN) || session.getAttribute("role").equals(Users.SAFETY_ENGINEER)) {%>
                <%@ include file="menus/staff.jsp" %>
                <%@ include file="menus/instructions.jsp" %>
                <%}  %>
                <% if (session.getAttribute("role").equals(Users.DEPARTMENT_HEAD)) {%>
                <%@ include file="menus/instructions.jsp" %>
                <%}  %>

                <li><a href="main.jsp" class="">Главная<span class="fa fa-angle-down"></span></a>
                    <ul class="submenu">
                        <li><a href="">меню второго уровня</a>
                        </li>

                        <li><a href="">меню второго уровня<span class="fa fa-angle-down"></span></a>
                            <ul class="submenu">
                                <li><a href="">меню третьего уровня</a></li>
                                <li><a href="">меню третьего уровня</a></li>
                                <li><a href="">меню третьего уровня</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li><a href="${pageContext.request.contextPath}login.jsp">Логин</a></li>
                <li><a href="contacts.jsp">Контакты</a></li>
                <li><a href="/logout">Выход</a></li>
            </ul>
        </nav>
</header>

<div>
    Ваш уровень доступа:<i><%= session.getAttribute("role").toString().toLowerCase() %></i>
</div>

  <legend>Форма ввода</legend>
    <style>
       .space {
         color: black;
         margin-left: 10px;
       }
    </style>
    <form action="addUser" method="post">

        <label  class="space">введите фамилию</label>
        <input style="margin-left:26px" width="400" name = "lastName" type="text"
               required value="фамилия" onBlur="if(this.value=='')this.value='фамилия'"
               onFocus="if(this.value=='фамилия')this.value='' "><br>

        <label class="space">введите имя</label>
        <input style="margin-left:70px" width="400" name="firstName" type="text"
               required value="имя" onBlur="if(this.value=='')this.value='имя'"
               onFocus="if(this.value=='имя')this.value='' "><br>

        <label class="space">введите отчество</label>
        <input style="margin-left:31px" width="400" name="middleName" type="text"
               required value="отчество" onBlur="if(this.value=='')this.value='отчество'"
               onFocus="if(this.value=='отчество')this.value='' "><br>

        <label class="space">введите Email</label>
        <input style="margin-left:61px" width="400" name="email" type="email"
               required value="email" onBlur="if(this.value=='')this.value='email'"
               onFocus="if(this.value=='email')this.value='' "><br>

        <label class="space">введите профессию</label>
        <input style="margin-left:13px" width="400" name="profession" type="text"
               required value="профессия" onBlur="if(this.value=='')this.value='профессия'"
               onFocus="if(this.value=='профессия')this.value='' "><br>

        <label class="space">выбор отдела</label><br>
        <select style="margin-left:184px" class="space" access="false" multiple="false" name="department">
            <option value="Administration" id="select-0">Администрация</option>
            <option value="Bookkeeping" id="select-1">Бухгалтерия</option>
            <option value="EngineeringGroup" id="select-2">Инженерная группа</option>
            <option value="Production" id="select-3">Производство</option>
        </select>
        <br>

        <input style="margin-left:228px" class="space" formaction="/addUser" type="submit" value="ДОБАВИТЬ">
    </form>
</body>
</html>
