<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />

</head>
<body>

<div id="login-form">
    <h1>Авторизация на сайте</h1>

    <fieldset>
        <form action="auth" method="post">
            <input name = "login"    type="text"     required value="Логин"  onBlur="if(this.value=='')this.value='Логин'"  onFocus="if(this.value=='Логин')this.value='' ">
            <input name = "password" type="password" required value="Пароль" onBlur="if(this.value=='')this.value='Пароль'" onFocus="if(this.value=='Пароль')this.value='' ">
            <input type="submit" value="ВОЙТИ">
            <footer class="clearfix">

                <p><span class="info">?</span><a href="#">Забыли пароль?</a></p>
            </footer>
        </form>
    </fieldset>
</div>
</body>
</html>