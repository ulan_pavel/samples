package com.ulan.services.impl;

import com.ulan.model.User;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class UserServiceTest {


    @Test
    public void saveUser() {
        UserService service = new UserService();
        User user = new User();
        user.setId(1L);
        user.setLogin("testlogin");
        user.setPassword("testpassword");
        user.setRole("admin");
        service.saveUser(user);


        Date date = new Date();
        java.sql.Date sqlDate1 = new java.sql.Date(Calendar.getInstance().getTimeInMillis());

        User user1 = new User("testLogin2", "testPass2", sqlDate1);//default в sql не работает, пришлось использовать конструктор
        service.saveUser(user1);

    }


    @Test
    public void findUser() {
        UserServiceTest test = new UserServiceTest();
        test.saveUser();
        UserService service = new UserService();
        System.out.println(service.findUser(2L));
    }

    @Test
    public void deleteUser() {
        UserServiceTest test = new UserServiceTest();
        test.saveUser();
        UserService service = new UserService();
        service.deleteUser(1L);
    }

    @Test
    public void updateUser() {
        UserServiceTest test = new UserServiceTest();
        test.saveUser();
        UserService service = new UserService();
        User user = new User();
        user.setId(1L);
        user.setLogin("updatelogin");
        user.setPassword("updatepassword");
        user.setRole("admin");
        service.updateUser(user);

    }

    @Test
    public void findAllUsers() {
        UserService service = new UserService();
        service.findAllUsers().forEach(user -> System.out.println(user.toString()));
    }
}