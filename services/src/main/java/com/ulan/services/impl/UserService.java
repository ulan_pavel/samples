package com.ulan.services.impl;

import com.ulan.dao.impl.UserDao;
import com.ulan.model.User;

import java.util.List;

public class UserService {
    private UserDao userDao = new UserDao();

    public UserService() {
    }

    public User findUser(Long id) {
        return userDao.get(id);
    }

    public void saveUser(User user) {
        userDao.create(user);
    }

    public void deleteUser(Long id) {
        userDao.delete(id);
    }

    public void updateUser(User user) {
        userDao.update(user);
    }

    public List<User> findAllUsers() {
        return userDao.getAll();
    }

}
