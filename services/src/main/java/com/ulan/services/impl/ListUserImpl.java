package com.ulan.services.impl;

import com.ulan.dao.impl.EmployeeDAOImpl;
import com.ulan.model.Employee;
import com.ulan.services.ListUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class ListUserImpl implements ListUser<Employee> {
    private static final Logger LOGGER = LogManager.getLogger(ListUserImpl.class.getName());

    @Override
    public List<Employee> listUser() {

        EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl();
        try {
            LOGGER.info("method employeeDAO.getAll in public class ListUserImpl implements ListUsers {}", "was done successful");
            return employeeDAO.getAll();
        } catch (SQLException e) {
            LOGGER.error("error during method employeeDAO.add in public class ListUserImpl implements ListUser<Employee> ", e);
            e.printStackTrace();
        }
        return null;
    }

    //FIXME work wrong
    public String listUserForView() {
        StringBuilder stringUserForView = new StringBuilder();
        List<Employee> result;
        result=listUser();
        for (Object i: result
             ) {
            stringUserForView.append("<li>").append(i.toString()).append("</li> \n");
        }
        return stringUserForView.toString();
    }


}
