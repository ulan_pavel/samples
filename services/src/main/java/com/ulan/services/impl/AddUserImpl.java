package com.ulan.services.impl;

import com.ulan.dao.EmployeeDAO;
import com.ulan.dao.impl.EmployeeDAOImpl;
import com.ulan.model.Employee;
import com.ulan.services.AddUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class AddUserImpl implements AddUser<Employee> {
    private static final Logger LOGGER = LogManager.getLogger(AddUserImpl.class.getName());

    @Override
    public boolean addUser(Employee employee) {
        EmployeeDAO<Employee> employeeDAO = new EmployeeDAOImpl();
        try {
            employeeDAO.add(employee);
        } catch (SQLException e) {
            LOGGER.error("error during method employeeDAO.add in public class AddUserImpl implements AddUser", e);
            e.printStackTrace();
            return false;
        }

        LOGGER.info("method employeeDAO.add in public class AddUserImpl implements AddUser {}", "was done successful");
        return true;
    }
}
