package com.ulan.services.impl;

import com.ulan.model.Contact;
import com.ulan.model.Users;
import com.ulan.services.AuthorizationService;

public class AuthorizationServiceImpl implements AuthorizationService {

    public Contact authorize(String login, String password) {

        switch (login) {
            case "Users.ADMIN":
                if ("admin".equals(password)) {
                    return new Contact(1L, login, password, Users.ADMIN);
                }
                break;
            case "Users.DEPARTMENT_HEAD":
                if ("department".equals(password)) {
                    return new Contact(2L, login, password, Users.DEPARTMENT_HEAD);
                }
                break;
            case "Users.SAFETY_ENGINEER":
                if ("personal".equals(password)) {
                    return new Contact(3L, login, password, Users.SAFETY_ENGINEER);
                }
                break;
            case "Users.VIEWER":
                if ("personal".equals(password)) {
                    return new Contact(4L, login, password, Users.VIEWER);
                }
                break;
        }
        return null;
    }
}



