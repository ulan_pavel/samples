package com.ulan.services;

import java.sql.SQLException;
import java.util.List;

public interface ListUser<T> {
    List<T> listUser() throws SQLException;
}
