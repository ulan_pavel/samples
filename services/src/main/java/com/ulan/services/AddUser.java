package com.ulan.services;

public interface AddUser<T> {
    boolean addUser(T t);
}
