package com.ulan.services;

import com.ulan.model.Contact;

public interface AuthorizationService {

     Contact authorize(String login, String password);
}
