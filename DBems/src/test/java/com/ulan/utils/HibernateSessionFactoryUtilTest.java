package com.ulan.utils;

import org.hibernate.SessionFactory;
import org.junit.Test;

public class HibernateSessionFactoryUtilTest {

    @Test
    public void getSessionFactory() {
        SessionFactory session = HibernateSessionFactoryUtil.getSessionFactory();
        session.close();
    }
}