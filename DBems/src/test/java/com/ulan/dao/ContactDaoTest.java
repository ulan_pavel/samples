package com.ulan.dao;

import com.ulan.dao.impl.ContactDao;
import com.ulan.model.Contact;
import com.ulan.model.Users;
import org.junit.Assert;


public class ContactDaoTest {
    public static void main(String[] args) throws Exception {
        InterfaceDao<Contact> contactDao = new ContactDao();
        System.out.println(contactDao.get(1L).toString());
        Contact contact = new Contact(1L,"admin","admin", Users.ADMIN);
        Assert.assertEquals(contactDao.get(1L).toString(), contact.toString());


    }
}