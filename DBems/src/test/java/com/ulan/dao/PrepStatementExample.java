package com.ulan.dao;

import com.ulan.JdbcProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class PrepStatementExample {
    private static final String CONTACT_BY_NAME = "select * from employee";

    public static void main(String[] args) throws Exception {

        Connection connection = JdbcProvider.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(CONTACT_BY_NAME);
        preparedStatement.executeQuery();
        preparedStatement.close();
        connection.close();

    }
}
