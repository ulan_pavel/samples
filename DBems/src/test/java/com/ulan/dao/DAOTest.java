package com.ulan.dao;

import com.ulan.dao.impl.ContactDao;
import com.ulan.model.Contact;
import com.ulan.model.Users;

import java.util.List;

public class DAOTest {
    public static void main(String[] args) throws Exception {
        InterfaceDao<Contact> contactDao = new ContactDao();
        List<Contact> all= contactDao.getAll();
        //FIXME убрать
        Contact contact = new Contact();
        contact.setLogin("jojo");
        contact.setPassword("jojopass");
        contact.setRole(Users.ADMIN);
        contactDao.create(contact);
        //
        all.stream()
                .map(Contact::getId)
                .forEach(System.out::println);
    }
}
