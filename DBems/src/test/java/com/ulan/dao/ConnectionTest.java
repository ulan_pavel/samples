package com.ulan.dao;

import com.ulan.JdbcProvider;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectionTest {
    public static void main(String[] args) throws Exception {
        Connection connection= JdbcProvider.getInstance().getConnection();
        //загнать в try{} и убрать clous'ы
        //connection.commit();
        DatabaseMetaData metaData = connection.getMetaData();
        String result = String.format(
                "%s %s %s",
                metaData.getURL(),
                metaData.getDatabaseProductName(),
                metaData.getDriverVersion()

        );
        System.out.println(result);

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from employee");
        while (resultSet.next()){
            String r= String.format(
                    "%s %s %s",
                    resultSet.getString(1),
                    resultSet.getString(6),
                    resultSet.getString(2)
            );
            System.out.println(r);
        }
        resultSet.close();
        statement.close();
        connection.close();
    }
}
