package com.ulan.dao.impl;

import com.ulan.dao.EmployeeDAO;
import com.ulan.model.Employee;
import javafx.util.converter.IntegerStringConverter;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class EmployeeDAOImplTest {
    @Ignore
    @Test
    public void add() throws SQLException {

        EmployeeDAO<Employee> employeeDAO = new EmployeeDAOImpl();
        //List<Contact> all= employeeDAO.getAll();
        //FIXME убрать
        Employee employee = new Employee("testFirstName", "testLastName", "testMiddleName", "Email@mail.ru", "Оператор", "Производство");
        //не срабатывает
        employee.setFirstName("testFirstName3");
        employee.setLastName("testLastName");
        employee.setMiddleName("testMiddleName");
        employee.setEmail("Email@mail.ru");
        employee.setProfession("Оператор");//добавляет только при условии, что профессия уже есть в таблице (причина - Fkey на таблицу Инструкции)
        employee.setDepartment("Производство");//аналогично (Fkey на производство)
        GregorianCalendar date = new GregorianCalendar();
        IntegerStringConverter converter = new IntegerStringConverter();
        //this.date=converter.toString(date.get(Calendar.YEAR))+"-"+converter.toString(date.get(Calendar.MONTH)+1)+"-"+converter.toString(date.get(Calendar.DAY_OF_MONTH));
        //Date date = new Date();
        employee.setDate(converter.toString(date.get(Calendar.YEAR)) + "-" + converter.toString(date.get(Calendar.MONTH) + 1) + "-" + converter.toString(date.get(Calendar.DAY_OF_MONTH)));

        System.out.println(employee.toString());
        boolean test = employeeDAO.add(employee);
        System.out.println(test);
        assert test;
    }

    @Test
    public void getAll() throws SQLException {
        EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl();
        System.out.println(employeeDAO.getAll().toString());

    }
}