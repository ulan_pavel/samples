-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: ems
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `id` int NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'admin','admin','ADMIN'),(2,'viewer','viewer','VIEWER'),(3,'head','head','DEPARTMENT_HEAD'),(4,'safe','safe','SAFETY_ENGINEER');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `position` (
  `profession` varchar(50) NOT NULL,
  `instruction` varchar(100) DEFAULT NULL,
  `department` varchar(50) NOT NULL,
  PRIMARY KEY (`profession`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` VALUES ('Генеральный директор','дир','Администрация'),('Главный бухгалтер','бух','Бухгалтерия'),('Главный инженер','инженер','Инженерная группа'),('Главный технолог','технолог','Производство'),('Механик-наладчик','механик','Инженерная группа'),('Оператор','оператор','Производство'),('Секретарь','секретарь','Администрация');
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protective_equipment`
--

DROP TABLE IF EXISTS `protective_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `protective_equipment` (
  `department` varchar(50) NOT NULL,
  `equipment` varchar(50) DEFAULT NULL,
  `shoes` varchar(50) DEFAULT NULL,
  `hat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`department`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protective_equipment`
--

LOCK TABLES `protective_equipment` WRITE;
/*!40000 ALTER TABLE `protective_equipment` DISABLE KEYS */;
INSERT INTO `protective_equipment` VALUES ('Администрация','не положено',NULL,NULL),('Бухгалтерия','не положено',NULL,NULL),('Инженерная группа','комплект_1','ботинки','каска_бел'),('Производство','комплект_2','ботинки','каска_желт');
/*!40000 ALTER TABLE `protective_equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `profession` varchar(50) NOT NULL,
  `date` date DEFAULT NULL,
  `department` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `employee_fk` (`department`),
  KEY `employee_fk_1` (`profession`),
  CONSTRAINT `employee_fk` FOREIGN KEY (`department`) REFERENCES `protective_equipment` (`department`),
  CONSTRAINT `employee_fk_1` FOREIGN KEY (`profession`) REFERENCES `position` (`profession`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,NULL,'Митянов','Алексей','Александрович','Генеральный директор','2015-02-20','Администрация'),(2,NULL,'Сурмина','Татьяна','Владимировна','Секретарь','2020-03-20','Администрация'),(3,NULL,'Гудкова','Ирина','Константиновна','Главный бухгалтер','2020-03-20','Бухгалтерия'),(4,NULL,'Укропов','Евгений','Валентинович','Главный инженер','2019-03-20','Инженерная группа'),(5,NULL,'Жданов','Андрей','Егорович','Механик-наладчик','2018-02-20','Инженерная группа'),(6,NULL,'Егоров','Станислав','Олегович','Механик-наладчик','2018-03-20','Инженерная группа'),(7,NULL,'Яроцкова','Вера','Григорьевна','Главный технолог','2009-01-20','Производство'),(8,NULL,'Клычников','Алексей','Анатольевич','Оператор','2003-05-20','Производство'),(9,NULL,'Крюков','Евгений','Львович','Оператор','2013-04-20','Производство'),(10,NULL,'Леонов','Владимир','Анатольевич','Оператор','2012-01-20','Производство');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ems'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-16 13:06:24
