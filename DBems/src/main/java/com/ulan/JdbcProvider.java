package com.ulan;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcProvider {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    private static final String PROPERTIES_FILE_NAME = "connection";

    private static final String JDBC_DRIVER_NAME = "driverName";
    private static final String JDBC_URL = "jdbcURL";
    private static final String JDBC_USERNAME = "username";
    private static final String JDBC_PASSWORD = "password";

    private static final JdbcProvider jdbcProvider = new JdbcProvider();

    private JdbcProvider() {
    }

    public static JdbcProvider getInstance() {
        return jdbcProvider;
    }

    public Connection getConnection() throws IOException, ClassNotFoundException, SQLException {
        LOGGER.info("Getting JDBC connection");
        InputStream in = JdbcProvider.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
        Properties props = new Properties();
        props.load(in);

        String jdbcDriverName = (String) props.get(JDBC_DRIVER_NAME);
        String jdbcUrl = (String) props.get(JDBC_URL);
        String jdbcUsername = (String) props.get(JDBC_USERNAME);
        String jdbcPassword = (String) props.get(JDBC_PASSWORD);

        Class.forName(jdbcDriverName);
        return DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);
    }
}
