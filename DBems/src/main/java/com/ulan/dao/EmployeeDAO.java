package com.ulan.dao;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeDAO<T> {

    boolean add(T record) throws SQLException;

    T get(Long id) throws SQLException;

    List<T> getAll() throws SQLException;

    boolean update (T record) throws SQLException;

    boolean delete (Long id) throws SQLException;


}
