package com.ulan.dao;

import java.sql.SQLException;
import java.util.List;

public interface InterfaceDao<T> {

    boolean create (T record) throws SQLException; //create;

    T get(Long id) throws SQLException;

    List<T> getAll() throws Exception;

    boolean update(T record) throws SQLException;

    boolean delete(Long id) throws SQLException;


}
