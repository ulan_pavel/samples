package com.ulan.dao.impl;

import com.ulan.dao.InterfaceDao;
import com.ulan.model.User;
import com.ulan.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class UserDao implements InterfaceDao<User> {

    @Override
    public boolean create(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.save(user);
            transaction.commit();
        } catch (Throwable e) {
            transaction.rollback();
            e.printStackTrace();
            session.close();
            return false;
        }
        session.close();
        return true;
    }

    @Override
    public User get(Long id) {

        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(User.class, id);
    }

    @Override
    public List<User> getAll() {
        List<User> users = (List<User>) HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From User").list();
        return users;
    }

    @Override
    public boolean update(User user) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.update(user);
            transaction.commit();
        } catch (Throwable e) {
            transaction.rollback();
            e.printStackTrace();
            session.close();
            return false;
        }
        session.close();
        return true;
    }

    @Override
    public boolean delete(Long id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.createQuery("delete from User where id= :id").executeUpdate();
            transaction.commit();
        } catch (Throwable e) {
            transaction.rollback();
            e.printStackTrace();
            session.close();
            return false;
        }
        session.close();
        return true;
    }

}
