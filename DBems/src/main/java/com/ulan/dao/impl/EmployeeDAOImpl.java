package com.ulan.dao.impl;

import com.ulan.JdbcProvider;
import com.ulan.dao.EmployeeDAO;
import com.ulan.model.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO<Employee> {
    private static final Logger LOGGER = LogManager.getLogger(EmployeeDAOImpl.class.getName());

    @Override
    public boolean add(Employee record) {

        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "insert into employee (firstName, lastName, middleName, email, profession, department) values (?,?,?,?,?,?)")) {
                statement.setString(1, record.getFirstName());
                statement.setString(2, record.getLastName());
                statement.setString(3, record.getMiddleName());
                statement.setString(4, record.getEmail());
                statement.setString(5, record.getProfession());
                statement.setString(6, record.getDepartment());

                int i = statement.executeUpdate();
                if (i == 1) {
                    LOGGER.info("add contact to table {} {}", "сотрудники", "successful");
                    return true;
                } else {
                    LOGGER.error("Can't add contact to table {}", "сотрудники");
                }
                try (ResultSet resultSet = statement.executeQuery("select * from  employee")) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public Employee get(Long id) throws SQLException {
        return null;
    }

    @Override
    public List<Employee> getAll() throws SQLException {
        List<Employee> result = new ArrayList<>();
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("select * from  employee")) {
                    while (resultSet.next()) {
                        LOGGER.info("reading list of contacts from table сотрудники");
                        Employee employee = new Employee();
                        employee.setFirstName(resultSet.getString("firstName"));
                        employee.setLastName(resultSet.getString("lastName"));
                        employee.setMiddleName(resultSet.getString("middleName"));
                        employee.setEmail(resultSet.getString("email"));
                        employee.setProfession(resultSet.getString("profession"));
                        employee.setDepartment(resultSet.getString("department"));

                        result.add(employee);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Can't get all contact {}", "!!!!", e);
        }
        LOGGER.info("returning the List of employers");
        return result;
    }

    @Override
    public boolean update(Employee record) throws SQLException {
        return false;
    }


    @Override
    public boolean delete(Long id) throws SQLException {
        return false;
    }
}
