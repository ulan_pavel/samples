package com.ulan.dao.impl;

import com.ulan.JdbcProvider;
import com.ulan.dao.InterfaceDao;
import com.ulan.model.Contact;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ContactDao implements InterfaceDao<Contact> {
    private static final Logger LOGGER = LogManager.getLogger(ContactDao.class.getName());

    @Override
    public boolean create(Contact record) throws SQLException {
        List<Contact> result = new ArrayList<>();
        try(Connection connection= JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "insert into contact (login, password, role) values (?,?,?)")) {
                statement.setString(1, record.getLogin());
                statement.setString(2, record.getPassword());
                statement.setString(3, record.getRole().toString());

                int i = statement.executeUpdate();
                if (i == 1) {
                } else {
                    throw new Exception();
                }

                try (ResultSet resultSet = statement.executeQuery("select * from  contact")) {
                }
            }
        } catch (Exception e) {
            LOGGER.error("Can't get all contact {}", "!", e);

        }

        return false;
    }

    @Override
    public Contact get(Long id) throws SQLException{

        try(Connection connection= JdbcProvider.getInstance().getConnection()) {

            try (PreparedStatement statement = connection.prepareStatement("select * from  contact where id=? ")) {
                statement.setLong (1, id);

                try (ResultSet resultSet = statement.executeQuery()) {

                    if (resultSet.next()) {
                        String login = resultSet.getString("login");
                        String password = resultSet.getString("password");
                        String role = resultSet.getString("role");

                        Contact contact = new Contact();
                        contact.setId(id);
                        contact.setLogin(login);
                        contact.setPassword(password);
                        contact.setRole(role);

                        return contact;
                    } else {
                        LOGGER.error("Can't get the contact ");

                    }
                }
            }
        } catch (Exception e) {//перенести в логгер
            LOGGER.error("Can't get all contact {}","!!!", e);
        }
        return null;
    }

    @Override
    public List<Contact> getAll() throws Exception {

        List<Contact> result = new ArrayList<>();
        try(Connection connection= JdbcProvider.getInstance().getConnection()) {

            try (Statement statement = connection.createStatement()) {

                try (ResultSet resultSet = statement.executeQuery("select * from  contact")) {
                    while (resultSet.next()) {
                        LOGGER.info("reading new contact");
                        Contact contact = new Contact(null,null,null,null);
                        contact.setId(resultSet.getLong("id"));
                        contact.setLogin(resultSet.getString("Login"));
                        contact.setPassword(resultSet.getString("Password"));
                        contact.setRole(resultSet.getString("Role"));

                        result.add(contact);
                    }
                }
            }
        } catch (Exception e) {//перенести в логгер
            LOGGER.error("Can't get all contact {}", "!!", e);
            throw new DAOException("error during get all");
        }

        return result;
    }

    @Override
    //FIXME доработать метод
    public boolean update(Contact record) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        List<Contact> result = new ArrayList<>();
        try(Connection connection= JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "delete FROM contact WHERE id=(?)")) {
                statement.setLong(1, id);

                int i = statement.executeUpdate();
                if (i == 1) {
                    return true;
                } else {
                    LOGGER.error("Can't delete the contact with id={}", id);
                    throw new DAOException("Can't delete the contact with id=" + id);
                }

            }
        } catch (Exception e) {
        }
        return false;
    }
}

class DAOException extends Exception {

    public DAOException(String message) {
        super(message);
    }
}